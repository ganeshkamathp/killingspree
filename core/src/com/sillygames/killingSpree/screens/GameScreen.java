package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.networking.GameClient;
import com.sillygames.killingSpree.networking.GameServer;
import com.sillygames.killingSpree.managers.WorldManager;
import com.sillygames.killingSpree.managers.WorldRenderer;
import com.sillygames.killingSpree.physics.WorldDebugRenderer;
import com.sillygames.killingSpree.networking.ClientStateProcessor;

public class GameScreen extends ScreenAdapter {


    private KillingSpree game;
    private WorldManager manager;
    private GameServer server;
    private WorldRenderer renderer;
    private GameClient client;
    private ClientStateProcessor clientStateProcessor;
    private boolean DEBUG = false;

    boolean allowConnections;

    public GameScreen(KillingSpree game) {
        this.game = game;
        allowConnections = false;
    }

    public boolean configureGame(boolean isServer,
                                 boolean allowConnections, String level,
                                 String host, String name,
                                 String currentPlayerSprite) {
        this.allowConnections = allowConnections;

        client = new GameClient(host, game.platformServices, isServer);
        clientStateProcessor = new ClientStateProcessor(client,
                game.audioPlayer, game.platformServices);

        if (isServer) {
            server = new GameServer(allowConnections);
            manager = new WorldManager(server, allowConnections);
            manager.loadLevel(level);
            server.setOutgoingQueue(manager.getOutgoingEventQueue());
            client.setWorldEventsQueue(manager.getIncomingEventQueue());
            manager.setOutgoingEventListener(clientStateProcessor);
        }

        if (!client.isConnected()) {
            return false;
        }

        renderer = new WorldRenderer(clientStateProcessor, game,
                name, currentPlayerSprite);

        if (DEBUG && isServer) {
            renderer.setDebugRenderer(
                    new WorldDebugRenderer(manager.getWorld()));
        }

        return true;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (manager != null) {
            manager.update(delta);
        }
        renderer.render(delta);

        if (clientStateProcessor.disconnected) {
            game.platformServices.toast("Disconnected");
            game.setScreen(new MainMenuScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        if (manager != null) {
            manager.dispose();
        }
        if (renderer != null) {
            renderer.dispose();
        }
        if (server != null) {
            server.stop();
        }
        if (client != null) {
            client.stop();
        }

    }

}
