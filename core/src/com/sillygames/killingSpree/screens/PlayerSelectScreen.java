package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.helpers.MyButton;
import com.sillygames.killingSpree.helpers.PopupTextField;
import com.sillygames.killingSpree.pool.AssetLoader;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class PlayerSelectScreen extends ScreenAdapter {

    private BitmapFont font;
    private FitViewport viewport;
    private MyButton currentButton;
    private KillingSpree game;
    private Stage stage;
    private Table table;
    private TextButton.TextButtonStyle textButtonStyle;
    ControllerListener controllerListener;
    private ArrayList<String> players;
    private int currentPlayer;
    private Sprite playerSprite;
    private String map;
    private boolean isServer;
    private String host;
    private boolean allowConnections;

    public PlayerSelectScreen(KillingSpree game) {
        this.game = game;
    }

    @Override
    public void show() {

        font = game.getFont(130);
        viewport = new FitViewport(1280, 720);

        table = new Table();
        table.setFillParent(true);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        addAllButtons();

        stage = new Stage(viewport);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (InputController.instance.buttonA()) {
                    currentButton.pressed = true;
                }
                currentButton = currentButton.process();
                return super.keyDown(event, keycode);
            }
        });

        controllerListener = new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonCode) {
                currentButton = currentButton.process();
                return false;
            }

            @Override
            public boolean axisMoved(Controller controller, int axisCode, float value) {
                currentButton = currentButton.process();
                return false;
            }

        };
        Controllers.addListener(controllerListener);

        final Preferences prefs = Gdx.app.getPreferences("profile");
        String name = prefs.getString("name");
        name = name.trim();
        if (name.length() == 0) {
            PopupTextField namePopup = new PopupTextField("profile",
                    "name", PopupTextField.Type.STRING);
            Gdx.input.getTextInput(namePopup, "", name, "");
        }
        players = new ArrayList<String>();
        populatePlayers(players);
        playerSprite = new Sprite();
        setPlayer(0);

    }

    public void configureServer(String map, boolean allowConnections) {
        this.map = map;
        isServer = true;
        this.allowConnections = allowConnections;
    }

    public void configureClient(String host) {
        this.host = host;
        isServer = false;
    }

    private void setPlayer(int index) {
        currentPlayer = index;
        Texture texture = AssetLoader.instance.
                getTexture(players.get(currentPlayer));
        TextureRegion playerIdle = TextureRegion.split(texture,
                texture.getWidth() / 10, texture.getHeight())[0][1];
        playerSprite.setRegion(playerIdle);
        playerSprite.setPosition(600, 400);
        playerSprite.setSize(playerIdle.getRegionWidth(),
                playerIdle.getRegionHeight());
        playerSprite.setScale(2);
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void populatePlayers(ArrayList<String> players) {
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".png") && name.contains("player");
            }
        };
        FileHandle[] playerList = Gdx.files.internal("sprites/").list(filter);
        for (FileHandle player: playerList) {
            players.add("sprites/" + player.name());
        }
    }

    public MyButton addButton(String text) {
        MyButton button = new MyButton(text, textButtonStyle);
        table.add(button);
        return button;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        viewport.apply();
        stage.getBatch().begin();
        playerSprite.draw(stage.getBatch());
        stage.getBatch().end();
        stage.act(delta);
        stage.draw();
        if (InputController.instance.closeButton()) {
            Screen mainMenuScreen = new MainMenuScreen(game);
            game.setScreen(mainMenuScreen);
        }
    }

    private void addAllButtons() {
        MyButton previousButton = addButton("  <<<  ");
        previousButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                if (currentPlayer != 0) {
                    currentPlayer -= 1;
                    setPlayer(currentPlayer);
                }
            }
        });
        MyButton selectPlayerButton = addButton("Select");
        selectPlayerButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                startGame(map, players.get(currentPlayer));
            }
        });
        selectPlayerButton.setActive(true);

        MyButton nextButton = addButton("  >>>  ");
        nextButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                if (currentPlayer != players.size() - 1) {
                    currentPlayer += 1;
                    setPlayer(currentPlayer);
                }
            }
        });

        selectPlayerButton.setEast(previousButton);
        selectPlayerButton.setWest(nextButton);

        currentButton = selectPlayerButton;
    }

    private void startGame(String map, String currentPlayerSprite) {
        addSmallDelay();
        Preferences prefs = Gdx.app.getPreferences("profile");
        GameScreen gameScreen = new GameScreen(game);
        String name = prefs.getString("name");
        if (isServer) {
            gameScreen.configureGame(true, allowConnections, map,
                    "", name, currentPlayerSprite);
        } else {
            gameScreen.configureGame(false, false, "", host, name,
                    currentPlayerSprite);
        }
        game.setScreen(gameScreen);
    }

    private void addSmallDelay() {
        try {
            // For Controllers
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        Controllers.removeListener(controllerListener);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        stage.setViewport(viewport);
    }
}
