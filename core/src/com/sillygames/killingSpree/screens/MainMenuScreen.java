package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.helpers.MyButton;
import com.sillygames.killingSpree.helpers.PopupTextField;

public class MainMenuScreen extends ScreenAdapter {

    private BitmapFont font;
    private FitViewport viewport;
    private MyButton currentButton;
    private KillingSpree game;
    private Stage stage;
    private Table table;
    private TextButton.TextButtonStyle textButtonStyle;
    ControllerListener controllerListener;
    
    public MainMenuScreen(KillingSpree game) {
        this.game = game;
    }

    @Override
    public void show() {
        font = game.getFont(130);
        viewport = new FitViewport(1280, 720);

        table = new Table();
        table.setFillParent(true);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        addAllButtons();

        stage = new Stage(viewport);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (InputController.instance.buttonA()) {
                    currentButton.pressed = true;
                }
                currentButton = currentButton.process();
                return super.keyDown(event, keycode);
            }
        });

        controllerListener = new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonCode) {
                currentButton = currentButton.process();
                return false;
            }

            @Override
            public boolean axisMoved(Controller controller, int axisCode, float value) {
                currentButton = currentButton.process();
                return false;
            }

        };
        Controllers.addListener(controllerListener);

        final Preferences prefs = Gdx.app.getPreferences("profile");
        String name = prefs.getString("name");
        name = name.trim();
        if (name.length() == 0) {
            PopupTextField namePopup = new PopupTextField("profile",
                    "name", PopupTextField.Type.STRING);
            Gdx.input.getTextInput(namePopup, "", name, "");

            Preferences scalingPrefs =
                    Gdx.app.getPreferences("settings");
            scalingPrefs.putInteger("scaling", 100);
            scalingPrefs.flush();
        }
    }

    public MyButton addButton(String text) {
        MyButton button = new MyButton(text, textButtonStyle);
        table.add(button);
        table.row();
        return button;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    private void addAllButtons() {
        MyButton startGameButton = addButton("Start game");
        startGameButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                startServer(false);
            }
        });

        MyButton joinGameButton = addButton("Join game");
        joinGameButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new ClientDiscoveryScreen(game));
            }
        });
        MyButton practiceButton = addButton("Solo practice");
        practiceButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                startServer(true);
            }
        });

        MyButton optionsButton = addButton("Options");
        optionsButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new OptionsScreen(game));
            }
        });

        startGameButton.setActive(true);
        currentButton = startGameButton;
        joinGameButton.setNorth(startGameButton);
        practiceButton.setNorth(joinGameButton);
        practiceButton.setSouth(optionsButton);
        startGameButton.setNorth(optionsButton);
    }

    private void startServer(boolean lonely) {
        if (lonely) {
            PlayerSelectScreen playerSelectScreen = new PlayerSelectScreen(game);
            playerSelectScreen.configureServer("maps/retro-small.tmx", false);
            game.setScreen(playerSelectScreen);
        } else {
            LevelSelectScreen levelSelectScreen = new LevelSelectScreen(game);
            game.setScreen(levelSelectScreen);
        }
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        Controllers.removeListener(controllerListener);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        stage.setViewport(viewport);
    }
}
