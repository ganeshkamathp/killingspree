package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.helpers.MyButton;
import com.sillygames.killingSpree.helpers.PopupTextField;

public class OptionsScreen extends ScreenAdapter {

    private BitmapFont font;
    private FitViewport viewport;
    private MyButton currentButton;
    private KillingSpree game;
    private Stage stage;
    private Table table;
    private TextButton.TextButtonStyle textButtonStyle;
    ControllerListener controllerListener;


    public OptionsScreen(KillingSpree game) {
        this.game = game;
    }

    @Override
    public void show() {
        font = game.getFont(130);
        viewport = new FitViewport(1280, 720);

        table = new Table();
        table.setFillParent(true);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        addAllButtons();

        stage = new Stage(viewport);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (InputController.instance.buttonA()) {
                    currentButton.pressed = true;
                }
                currentButton = currentButton.process();
                return super.keyDown(event, keycode);
            }
        });

        controllerListener = new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonCode) {
                currentButton = currentButton.process();
                return false;
            }

            @Override
            public boolean axisMoved(Controller controller, int axisCode, float value) {
                currentButton = currentButton.process();
                return false;
            }

        };
        Controllers.addListener(controllerListener);

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        Controllers.removeListener(controllerListener);
    }

    public MyButton addButton(String text) {
        MyButton button = new MyButton(text, textButtonStyle);
        table.add(button);
        table.row();
        return button;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if (InputController.instance.closeButton()) {
            Screen mainMenuScreen = new MainMenuScreen(game);
            game.setScreen(mainMenuScreen);
        }
    }

    private void addAllButtons() {
        MyButton changeNameButton = addButton("change name");
        changeNameButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                final Preferences prefs = Gdx.app.getPreferences("profile");
                String name = prefs.getString("name");
                name = name.trim();
                PopupTextField namePopup = new PopupTextField("profile",
                        "name", PopupTextField.Type.STRING);
                Gdx.input.getTextInput(namePopup, "", name, "");
            }
        });

        MyButton setControlsScaleButton  = addButton("set buttons scale");
        setControlsScaleButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                final Preferences prefs = Gdx.app.getPreferences("settings");
                int scaling = prefs.getInteger("scaling");
                PopupTextField scalingPopUp = new PopupTextField("settings",
                        "scaling", PopupTextField.Type.INTEGER);
                Gdx.input.getTextInput(scalingPopUp, "",
                        Integer.toString(scaling), "");
            }
        });

        MyButton backButton = addButton("back");
        backButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        changeNameButton.setActive(true);
        currentButton = changeNameButton;
        setControlsScaleButton.setNorth(changeNameButton);
        backButton.setNorth(setControlsScaleButton);
        backButton.setSouth(changeNameButton);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        stage.setViewport(viewport);
    }

}