package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.esotericsoftware.kryonet.Client;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.helpers.MyButton;
import com.sillygames.killingSpree.settings.Constants;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ClientDiscoveryScreen extends ScreenAdapter {

    private BitmapFont font;
    private FitViewport viewport;
    private MyButton currentButton;
    private MyButton backButton;
    private MyButton refreshButton;
    private MyButton manualIpButton;
    private Client client;
    private KillingSpree game;
    private Table buttonsTable;
    private ArrayList<MyButton> ipAddressButtons;
    private Stage stage;
    private TextButton.TextButtonStyle textButtonStyle;

    private ControllerListener controllerListener;

    public ClientDiscoveryScreen(KillingSpree game) {
        this.game = game;
    }

    @Override
    public void show() {
        client = new Client();
        client.start();
        font = game.getFont(80);
        viewport = new FitViewport(1280, 720);

        ipAddressButtons = new ArrayList<MyButton>();

        buttonsTable = new Table();
        buttonsTable.setFillParent(true);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;

        stage = new Stage(viewport);
        stage.addActor(buttonsTable);

        Gdx.input.setInputProcessor(stage);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (InputController.instance.buttonA()) {
                    currentButton.pressed = true;
                }
                currentButton = currentButton.process();
                return super.keyDown(event, keycode);
            }
        });

        controllerListener = new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonCode) {
                currentButton = currentButton.process();
                return false;
            }

            @Override
            public boolean axisMoved(Controller controller, int axisCode, float value) {
                currentButton = currentButton.process();
                return false;
            }

        };
        Controllers.addListener(controllerListener);

        addAllButtons();
        addIpButtons();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if (InputController.instance.closeButton()) {
            Screen mainMenuScreen = new MainMenuScreen(game);
            game.setScreen(mainMenuScreen);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        font = null;
        Controllers.removeListener(controllerListener);
    }
    

    public MyButton addButton(String text, boolean newRow, int colspan) {
        MyButton button = new MyButton(text, textButtonStyle);
        buttonsTable.add(button).colspan(colspan);
        if (newRow)
            buttonsTable.row();
        return button;
    }
    
    private void addAllButtons() {
        refreshButton = addButton("Refresh", false, 1);
        refreshButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                addIpButtons();
            }
        });
        backButton = addButton("Back", true, 1);
        backButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        manualIpButton = addButton("Enter IP", true, 2);
        currentButton = refreshButton;
        currentButton.setActive(true);
        refreshButton.setWest(backButton);
        manualIpButton.setNorth(backButton);
        manualIpButton.setNorth(refreshButton);
    }

    private void addIpButtons() {
        for (MyButton button: ipAddressButtons) {
            buttonsTable.removeActor(button);
        }
        ipAddressButtons.clear();
        game.platformServices.shortToast("Searching for servers");
        manualIpButton.south = null;
        MyButton previousButton = manualIpButton;
        List<InetAddress> tempAddresses = client.discoverHosts(Constants.DISCOVERY_UDP_PORT,
                Constants.TIMEOUT);
        if (tempAddresses.size() == 0) {
            game.platformServices.toast("no servers found");
        }

        for (final InetAddress address : tempAddresses) {
            MyButton button = new MyButton(address.getHostName(), textButtonStyle);
            button.setRunOnPress(new Runnable() {
                @Override
                public void run() {
                    joinGame(address.getHostName());
                }
            });
            previousButton.setSouth(button);
            previousButton = button;
            ipAddressButtons.add(button);
            buttonsTable.add(button);
            if (manualIpButton.south == null) {
                manualIpButton.south = button;
            }
        }
    }

    private void joinGame (final String host) {
//            GameScreen gameScreen = new GameScreen(game);
//            final Preferences prefs = Gdx.app.getPreferences("profile");
//            if (gameScreen.configureGame(false, false, "maps/retro-small.tmx", host,
//                    prefs.getString("name"))) {
//                game.setScreen(gameScreen);
//            } else {
//                gameScreen.dispose();
//            }
        PlayerSelectScreen playerSelectScreen = new PlayerSelectScreen(game);
        playerSelectScreen.configureClient(host);
        game.setScreen(playerSelectScreen);
    }

}
