package com.sillygames.killingSpree.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.helpers.MyButton;
import com.sillygames.killingSpree.helpers.PopupTextField;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class LevelSelectScreen extends ScreenAdapter {

    private BitmapFont font;
    private FitViewport viewport;
    private MyButton currentButton;
    private KillingSpree game;
    private Stage stage;
    private Table table;
    private TextButton.TextButtonStyle textButtonStyle;
    ControllerListener controllerListener;
    private ArrayList<String> maps;
    private int currentMap;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private FitViewport mapViewport;
    private OrthographicCamera mapCamera;

    public LevelSelectScreen(KillingSpree game) {
        this.game = game;
    }

    @Override
    public void show() {

        font = game.getFont(130);
        viewport = new FitViewport(1280, 720);

        table = new Table();
        table.setFillParent(true);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        addAllButtons();

        stage = new Stage(viewport);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (InputController.instance.buttonA()) {
                    currentButton.pressed = true;
                }
                currentButton = currentButton.process();
                return super.keyDown(event, keycode);
            }
        });

        controllerListener = new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonCode) {
                currentButton = currentButton.process();
                return false;
            }

            @Override
            public boolean axisMoved(Controller controller, int axisCode, float value) {
                currentButton = currentButton.process();
                return false;
            }

        };
        Controllers.addListener(controllerListener);

        final Preferences prefs = Gdx.app.getPreferences("profile");
        String name = prefs.getString("name");
        name = name.trim();
        if (name.length() == 0) {
            PopupTextField namePopup = new PopupTextField("profile",
                    "name", PopupTextField.Type.STRING);
            Gdx.input.getTextInput(namePopup, "", name, "");
        }
        maps = new ArrayList<String>();
        populateMaps(maps);
        initMapsLayer(0);

    }

    private void initMapsLayer(int index) {
        currentMap = index;
        map = new TmxMapLoader().load(maps.get(currentMap));
        renderer = new OrthogonalTiledMapRenderer(map);
        TiledMapTileLayer layer = (TiledMapTileLayer) map.
                getLayers().get("terrain");
        int mapWidth = (int) (layer.getTileWidth() * layer.getWidth());
        int mapHeight = (int) (layer.getTileHeight() * layer.getHeight());

        mapCamera = new OrthographicCamera(mapWidth, mapHeight);
        mapViewport = new FitViewport(mapWidth, mapHeight, mapCamera);
        mapCamera.setToOrtho(false, mapWidth, mapHeight);

        renderer.setView(mapCamera.combined, 0, 0,
                mapWidth, mapHeight);
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void populateMaps(ArrayList<String> maps) {
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".tmx");
            }
        };
        FileHandle[] mapList = Gdx.files.internal("maps/").list(filter);
        for (FileHandle map: mapList) {
            maps.add("maps/" + map.name());
        }
    }

    public MyButton addButton(String text) {
        MyButton button = new MyButton(text, textButtonStyle);
        table.add(button);
        return button;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapViewport.apply();
        renderer.render();
        viewport.apply();
        stage.act(delta);
        stage.draw();
        if (InputController.instance.closeButton()) {
            Screen mainMenuScreen = new MainMenuScreen(game);
            game.setScreen(mainMenuScreen);
        }
    }

    private void addAllButtons() {
        MyButton previousButton = addButton("  <<<  ");
        previousButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                if (currentMap != 0) {
                    currentMap -= 1;
                    map.dispose();
                    initMapsLayer(currentMap);
                }
            }
        });
        MyButton startGameButton = addButton("Start game");
        startGameButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                startServer(maps.get(currentMap));
            }
        });
        startGameButton.setActive(true);

        MyButton nextButton = addButton("  >>>  ");
        nextButton.setRunOnPress(new Runnable() {
            @Override
            public void run() {
                if (currentMap != maps.size() - 1) {
                    currentMap += 1;
                    map.dispose();
                    initMapsLayer(currentMap);
                }
            }
        });

        startGameButton.setEast(previousButton);
        startGameButton.setWest(nextButton);

        currentButton = startGameButton;
    }

    private void startServer(String map) {
        PlayerSelectScreen playerSelectScreen = new PlayerSelectScreen(game);
        playerSelectScreen.configureServer(map, true);
        game.setScreen(playerSelectScreen);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        Controllers.removeListener(controllerListener);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        mapViewport.update(width, height);
        mapCamera.update();
        stage.setViewport(viewport);
    }
}
