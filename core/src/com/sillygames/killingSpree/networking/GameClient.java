package com.sillygames.killingSpree.networking;

import com.esotericsoftware.kryonet.Client;
import com.sillygames.killingSpree.PlatformServices;
import com.sillygames.killingSpree.helpers.Event;
import com.sillygames.killingSpree.pool.MessageObjectPool;
import com.sillygames.killingSpree.settings.Constants;

import java.io.IOException;
import java.util.ArrayList;

public class GameClient extends Client {

    private boolean local;
    private ArrayList<Event> worldEvents;
    public GameClient(String host, PlatformServices services, boolean local) {
        super();
        if (!local) {
            NetworkRegisterer.register(this);
            start();
            try {
                connect(Constants.TIMEOUT, host,
                        Constants.GAME_TCP_PORT,
                        Constants.GAME_UDP_PORT);
            } catch (IOException e) {
                services.toast("Server not found");
                e.printStackTrace();
            }
        }
        this.local = local;
    }

    @Override
    public boolean isConnected() {
        return super.isConnected() || local;
    }

    public void setWorldEventsQueue(ArrayList<Event> worldEvents) {
        this.worldEvents = worldEvents;
    }

    @Override
    public int sendTCP(Object object) {
        if (local) {
            worldEvents.add(MessageObjectPool.instance.
                    eventPool.obtain().set(Event.State.RECEIVED, object));
            return 0;
        } else {
            return super.sendTCP(object);
        }
    }

    @Override
    public int sendUDP(Object object) {
        if (local) {
            worldEvents.add(MessageObjectPool.instance.
                eventPool.obtain().set(Event.State.RECEIVED, object));
            return 0;
        } else {
            return super.sendUDP(object);
        }
    }
}
