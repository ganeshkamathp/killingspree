package com.sillygames.killingSpree.networking.messages;

import com.sillygames.killingSpree.pool.Poolable;

public class DestroyObjectMessage implements Poolable {
    public short id;

    @Override
    public void reset() {
        id = -2;
    }
}
