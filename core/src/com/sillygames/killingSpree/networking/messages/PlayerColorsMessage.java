package com.sillygames.killingSpree.networking.messages;

import java.util.HashMap;

public class PlayerColorsMessage {

    public HashMap<Short, String> colors;

    public PlayerColorsMessage() {
        colors = new HashMap<Short, String>();
    }

}
