package com.sillygames.killingSpree.networking;

import com.badlogic.gdx.utils.TimeUtils;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.sillygames.killingSpree.PlatformServices;
import com.sillygames.killingSpree.clientEntities.ClientBomb;
import com.sillygames.killingSpree.clientEntities.ClientEntity;
import com.sillygames.killingSpree.clientEntities.ClientPlayer;
import com.sillygames.killingSpree.networking.messages.*;
import com.sillygames.killingSpree.pool.MessageObjectPool;
import com.sillygames.killingSpree.settings.Constants;
import com.sillygames.killingSpree.sound.SFXPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClientStateProcessor extends Listener{

    private static final int QUEUE_LENGTH = 6;
    private GameClient client;
    public ArrayList<GameStateMessage> stateQueue;
    public long timeOffset = 0;
    private GameStateMessage nextState;
    int lag = 0;
    AtomicBoolean wait;
    ConcurrentHashMap<Short, ClientEntity> worldMap;
    public boolean disconnected;
    public boolean gameOver;
    private SFXPlayer audioPlayer;
    private PlayerNamesMessage playerNames;
    private PlatformServices services;
    private long previousTime;
    public String level;
    public String currentOverlayMessage;
    private PlayerColorsMessage playerColorsMessage;

    public ClientStateProcessor(GameClient client, SFXPlayer audioPlayer, PlatformServices services) {
        this.client = client;
        client.addListener(this);
        this.services = services;
        
        nextState = MessageObjectPool.instance.
                gameStateMessagePool.obtain();
        nextState.time = 0;
        stateQueue = new ArrayList<GameStateMessage>();
        wait = new AtomicBoolean(false);
        disconnected = false;
        this.audioPlayer = audioPlayer;
        playerNames = new PlayerNamesMessage();
        playerColorsMessage = new PlayerColorsMessage();
        gameOver = false;
    }

    @Override
    public void received(Connection connection, Object object) {
        if (object instanceof GameStateMessage) {
            addNewState((GameStateMessage) object);
        } else if (object instanceof DestroyObjectMessage) {
            short id = ((DestroyObjectMessage) object).id;
            if(worldMap.get(id) != null ) {
                worldMap.get(id).destroy = true;
                //FIXME: Set remove in the client entity
                if (!(worldMap.get(id) instanceof ClientBomb))
                    worldMap.get(id).remove = true;
            }
        } else if (object instanceof AudioMessage) {
            audioPlayer.playAudioMessage((AudioMessage) object);
        } else if (object instanceof PlayerNamesMessage) {
            this.playerNames = (PlayerNamesMessage) object;
        } else if (object instanceof PlayerColorsMessage) {
            this.playerColorsMessage = (PlayerColorsMessage) object;
        } else if (object instanceof ServerStatusMessage) {
            ServerStatusMessage message = (ServerStatusMessage) object;
            services.toast(message.toastText);
            if (message.status == ServerStatusMessage.Status.DISCONNECT &&
                    !gameOver) {
                disconnected = true;
            }
        } else if (object instanceof String) {
            // FIXME: Move away from native types
            services.toast((String) object);
        } else if (object instanceof LoadMapMessage) {
            level = ((LoadMapMessage) object).map;
        } else if (object instanceof OverlayMessage) {
            currentOverlayMessage = ((OverlayMessage)object).message;
        } else if (object instanceof GameOverMessage) {
            gameOver = true;
        }
        super.received(connection, object);
    }

    public void processPlayers() {
        HashMap<Short, String> sprites = playerColorsMessage.colors;
        for (Short id: sprites.keySet()) {
            if (worldMap.containsKey(id)) {
                ((ClientPlayer) worldMap.get(id)).loadPlayerSprite(sprites.get(id));
            }
        }
        HashMap<Short, String> names = playerNames.players;
        for (Short id: names.keySet()) {
            if (worldMap.containsKey(id)) {
                ((ClientPlayer) worldMap.get(id)).setName(names.get(id));
            }
        }
    }

    @Override
    public void disconnected(Connection connection){
        disconnected = true;
    }
    
    public void addNewState(GameStateMessage state) {
        if (wait == null) {
            wait = new AtomicBoolean(false);
        }
        if (stateQueue == null) {
            stateQueue = new ArrayList<GameStateMessage>();
        }
        while (!wait.compareAndSet(false, true));
        
        if (stateQueue.size() == 0) {
            stateQueue.add(state); 
        }
        for (int i = stateQueue.size() - 1; i >= 0; i--) {
            if (stateQueue.get(i).time < state.time) {
                stateQueue.add(i + 1, state);
                break;
            }
        }
        wait.set(false);
    }
    
    public float processStateQueue() {
        float alpha = 0;
        long currentTime = TimeUtils.nanoTime();
        while (!wait.compareAndSet(false, true));
        if (stateQueue.size() < QUEUE_LENGTH) {
            wait.set(false);
            return 0;
        }
        
        while (stateQueue.size() > QUEUE_LENGTH) {
            stateQueue.remove(0);
        }
        
        long currentServerTime = currentTime + timeOffset;
        if (currentServerTime < stateQueue.get(0).time) {
            lag++;
            if(lag > 3) {
                lag = 0;
                timeOffset = stateQueue.get(QUEUE_LENGTH - 3).time - currentTime;
                currentServerTime = currentTime + timeOffset;
            }
        } else if (currentServerTime > stateQueue.get(QUEUE_LENGTH - 1).time) {
            lag++;
            if(lag > 3) {
                timeOffset -= (10000 * Math.pow(10, lag));
                currentServerTime = currentTime + timeOffset;
            }
        } else {
            lag = 0;
        }

        for (GameStateMessage state: stateQueue) {
            this.nextState = state;
            if (state.time > currentServerTime) {
                break;
            }
        }

        // Calculate the alpha for interpolation
        long nextTime = nextState.time;
        currentTime += timeOffset;
        wait.set(false);
        if (nextTime != previousTime)
            alpha = (float)(currentTime - previousTime) / (float)(nextTime - previousTime);
        if (currentTime > nextTime) {
            alpha = 1;
        }
//        if (DEBUG) {
//            Gdx.app.log("alpha", Float.toString(alpha));
//            Gdx.app.log("NextTime", Long.toString(nextTime));
//            Gdx.app.log("CurrentTime", Long.toString(currentTime));
//            Gdx.app.log("PreviousTime", Long.toString(previousTime));
//            Gdx.app.log("ServerTime", Long.toString(currentServerTime));
//            Gdx.app.log("Offset", Long.toString(timeOffset));
//            Gdx.app.log("index", Integer.toString(stateQueue.indexOf(nextState)));
//            Gdx.app.log("********", "********");
//        }

        previousTime = currentTime;

        return alpha;
    }
    
    public GameStateMessage getNextState() {
        return nextState;
    }

    public void setWorldMap(ConcurrentHashMap<Short,ClientEntity> worldMap) {
        this.worldMap = worldMap;
    }

    public void sendUDP(ControlsMessage message) {
        client.sendUDP(message);
    }

    public void sendClientDetails(String name, String playerSprite) {
        ClientDetailsMessage clientDetails = new ClientDetailsMessage();
        clientDetails.name = name;
        clientDetails.protocolVersion = Constants.PROTOCOL_VERSION;
        clientDetails.playerSprite = playerSprite;
        client.sendTCP(clientDetails);
    }

    public void disconnect() {
    }

}
