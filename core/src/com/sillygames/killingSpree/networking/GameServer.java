package com.sillygames.killingSpree.networking;

import com.esotericsoftware.kryonet.Server;
import com.sillygames.killingSpree.helpers.Event;
import com.sillygames.killingSpree.pool.MessageObjectPool;
import com.sillygames.killingSpree.settings.Constants;

import java.util.ArrayList;

public class GameServer extends Server{
    private boolean allowConnections;

    private ArrayList<Event> outgoingQueue;

    public GameServer(boolean allowConnections) {
        super();
        this.allowConnections = allowConnections;
        if (allowConnections) {
            NetworkRegisterer.register(this);
            start();
            try {
                bind(Constants.GAME_TCP_PORT,
                        Constants.GAME_UDP_PORT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {
        if (allowConnections) {
            super.stop();
        }
    }

    @Override
    public void sendToAllTCP(Object object) {
        outgoingQueue.add(MessageObjectPool.instance.
                eventPool.obtain().set(Event.State.RECEIVED, object));
        if (allowConnections) {
            super.sendToAllTCP(object);
        }
    }

    @Override
    public void sendToTCP(int connectionID, Object object) {
        if (connectionID != -1) {
            super.sendToTCP(connectionID, object);
        } else {
            outgoingQueue.add(MessageObjectPool.instance.
                    eventPool.obtain().set(Event.State.RECEIVED, object));
        }
    }

    public void setOutgoingQueue(ArrayList<Event> outgoingQueue) {
        this.outgoingQueue = outgoingQueue;
    }
}
