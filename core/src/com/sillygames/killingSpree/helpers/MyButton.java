package com.sillygames.killingSpree.helpers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.sillygames.killingSpree.controls.InputController;

public class MyButton extends TextButton {
    public MyButton north;
    public MyButton south;
    public MyButton east;
    public MyButton west;
    public boolean pressed;
    private Runnable runOnPress;

    public MyButton(String text, TextButtonStyle style){
        super(text, style);
        pressed = false;
        setActive(false);
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (runOnPress != null) {
                    runOnPress.run();
                }
                return true;
            }
        });
    }
    
    public void setActive(boolean active){
        if (InputController.instance.controllerEnabled() ||
                Gdx.app.getType() == Application.ApplicationType.Desktop) {
            setColor(1, 1, 1, active ? 1 : 0.3f);
        }
    }
    
    public void setNorth(MyButton north) {
        this.north = north;
        north.south = this;
    }
    
    public void setSouth(MyButton south) {
        this.south = south;
        south.north = this;
    }
    
    public void setEast(MyButton east) {
        this.east = east;
        east.west = this;
    }
    
    public void setWest(MyButton west) {
        this.west = west;
        west.east = this;
    }

    public MyButton process() {
        MyButton pressedButton = null;
        if (InputController.instance.axisDown()) {
            pressedButton = south;
        } else if (InputController.instance.axisUp()) {
            pressedButton = north;
        } else if (InputController.instance.axisLeft()) {
            pressedButton = east;
        } else if (InputController.instance.axisRight()) {
            pressedButton = west;
        } else if (InputController.instance.buttonA()) {
            if (runOnPress != null) {
                if (InputController.instance.controllerEnabled()) {
                    smallDelay(500);
                }
                runOnPress.run();
            }
        }
        if (pressedButton != null) {
            if (InputController.instance.controllerEnabled()) {
                smallDelay(200);
            }
            pressedButton.setActive(true);
            setActive(false);
            return pressedButton;
        }
        return this;
    }

    private void smallDelay(int milliseconds) {
        try {
            // For Controllers
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPressed() {
        return super.isPressed() || pressed;
    }

    public void setRunOnPress(Runnable buttonPress) {
        this.runOnPress = buttonPress;
    }
}
