package com.sillygames.killingSpree.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.Preferences;

public class PopupTextField implements TextInputListener{
    Preferences prefs;
    String field;
    Type type;
    public enum Type {STRING, FLOAT, INTEGER};

    public PopupTextField(String pref, String field, Type type) {
        this.prefs = Gdx.app.getPreferences(pref);
        this.field = field.trim();
        this.type = type;
    }

    @Override
    public void input(String text) {
        text = text.trim();
        if (type == Type.STRING) {
            prefs.putString(field, text);
        } else if (type == Type.FLOAT) {
            prefs.putFloat(field, Float.parseFloat(text));
        } else if (type == Type.INTEGER) {
            prefs.putInteger(field, Integer.parseInt(text));
        }
        prefs.flush();
    }

    @Override
    public void canceled() {
    }
}
