package com.sillygames.killingSpree.sound;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.sillygames.killingSpree.networking.messages.AudioMessage;

public class SFXPlayer {
    
    private Sound jump;
    private Sound shoot;
    private Sound explode;
    private Sound hurt;
    private Sound jumpedOn;
    AssetManager sounds;
    
    public SFXPlayer() {
        sounds = new AssetManager();
        sounds.load("sounds/jump.ogg", Sound.class);
        sounds.load("sounds/shoot.ogg", Sound.class);
        sounds.load("sounds/explode.ogg", Sound.class);
        sounds.load("sounds/hurt.ogg", Sound.class);
        sounds.load("sounds/jumpedon.ogg", Sound.class);
        sounds.finishLoading();
        jump = sounds.get("sounds/jump.ogg", Sound.class);
        shoot = sounds.get("sounds/shoot.ogg");
        explode = sounds.get("sounds/explode.ogg");
        hurt = sounds.get("sounds/hurt.ogg");
        jumpedOn = sounds.get("sounds/jumpedon.ogg");
    }

    public void jump() {
        jump.play();
    }
    
    public void shoot() {
        shoot.play();
    }
    
    public void hurt() {
        hurt.play();
    }

    public void explode() {
        explode.play();
    }
    
    public void jumpedOn() {
        jumpedOn.play();
    }

    public void dispose() {
        jump.dispose();
        shoot.dispose();
        hurt.dispose();
        explode.dispose();
        jumpedOn.dispose();
    }
    
    public void playAudioMessage(AudioMessage message) {
        if (message.getJump()) {
            jump();
        }
        if (message.getShoot()) {
            shoot();
        }
        if (message.getHurt()) {
            hurt();
        }
        if (message.getExplode()) {
            explode();
        }
        if (message.getJumpedOn()) {
            jumpedOn();
        }
    }

}
