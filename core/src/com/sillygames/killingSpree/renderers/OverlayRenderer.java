package com.sillygames.killingSpree.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class OverlayRenderer {
    private BitmapFont font;
    private int screenWidth;
    private int screenHeight;
    private ScreenViewport viewport;
    private GlyphLayout layout;
    private String previousOverlayText;
    private float textX;
    private float textY;
    private float textWidth;

    public OverlayRenderer() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.color = Color.GRAY;
        parameter.borderColor = Color.BLACK;
        parameter.shadowColor = Color.MAROON;
        parameter.shadowOffsetX = 3;
        parameter.shadowOffsetY = 3;
        parameter.borderWidth = 3;
        font = new FreeTypeFontGenerator
                (Gdx.files.internal("fonts/game.ttf")).generateFont(parameter);

        layout = new GlyphLayout();
        previousOverlayText = "";
        updateGlyphLayout(previousOverlayText);

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        viewport = new ScreenViewport();
        viewport.update(screenWidth, screenHeight);
    }

    public void render(SpriteBatch batch, String overlayText) {
        if (previousOverlayText != overlayText) {
            updateGlyphLayout(overlayText);
        }
        if (overlayText.length() != 0) {
            viewport.apply();
            batch.setProjectionMatrix(viewport.getCamera().combined);
            batch.begin();
            font.draw(batch, overlayText, textX, textY,
                    textWidth, Align.center, true);
            batch.end();
        }
    }

    private void updateGlyphLayout(String overlayText) {
        layout.setText(font, overlayText);
        textWidth = Math.min(screenWidth * 0.7f, layout.width);
        textX = -(textWidth * 0.5f);
        textY = (layout.height * 0.5f);
        previousOverlayText = overlayText;
    }

    public void dispose() {
        font.dispose();
    }

}
