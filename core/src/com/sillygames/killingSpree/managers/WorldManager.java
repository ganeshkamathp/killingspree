package com.sillygames.killingSpree.managers;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.sillygames.killingSpree.helpers.Event;
import com.sillygames.killingSpree.helpers.Event.State;
import com.sillygames.killingSpree.helpers.MyConnection;
import com.sillygames.killingSpree.networking.GameServer;
import com.sillygames.killingSpree.networking.messages.*;
import com.sillygames.killingSpree.networking.messages.ServerStatusMessage.Status;
import com.sillygames.killingSpree.physics.Body;
import com.sillygames.killingSpree.physics.World;
import com.sillygames.killingSpree.pool.MessageObjectPool;
import com.sillygames.killingSpree.settings.Constants;
import com.sillygames.killingSpree.serverEntities.*;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class WorldManager{
    
    private World world;
    private GameServer server;
    public ConcurrentHashMap<Integer, ServerPlayer> playerList;
    public ArrayList<ServerEntity> entities;
    private WorldManager worldManager = this;
    private ArrayList<Event> incomingEventQueue;
    private ArrayList<Event> outgoingEventQueue;
    private Listener serverListener;
    private Listener outgoingEventListener;
    private WorldBodyUtils worldBodyUtils;
    public MyConnection dummyConnection;
    public short id;
    public AudioMessage audio;
    public LevelLoader loader;
    private OverlayMessage currentOverlayMessage;
    public String level;
    private boolean pauseGame;
    private float runTime;
    private boolean allowConnections;
    private String gameOverMessage;
    private boolean gameOver;
    private short WINNING_SCORE = 10;

    public WorldManager(GameServer server, boolean allowConnections){
        gameOverMessage = "";

        playerList = new ConcurrentHashMap<Integer, ServerPlayer>();

        world = new World(new Vector2(0, -500f));

        serverListener = new WorldManagerServerListener();

        if  (server != null) {
            server.addListener(serverListener);
        }

        this.server = server;

        entities = new ArrayList<ServerEntity>();

        incomingEventQueue = new ArrayList<Event>();
        outgoingEventQueue = new ArrayList<Event>();
        dummyConnection = new MyConnection();
        audio = new AudioMessage();

        worldBodyUtils = new WorldBodyUtils(worldManager);

        id = 0;
        loader = new LevelLoader("maps/retro-small.txt", this, server);
        this.allowConnections = allowConnections;

        pauseGame = true;
        runTime = 0;

        currentOverlayMessage = new OverlayMessage();
        currentOverlayMessage.message = "";
        if (allowConnections) {
            currentOverlayMessage.message =
                    "----------------------\n" +
                    "Waiting for players!!!\n" +
                    "----------------------\n" +
                    "First to " + Integer.toString(WINNING_SCORE) + " wins!!!\n";
        }

        currentOverlayMessage.message +=

                "----------------------\n" +
                "Press jump\nto begin\n" +
                "----------------------\n";

        gameOver = false;
    }

    public void setOutgoingEventListener(Listener listener) {
        outgoingEventListener = listener;
    }

    public ServerFrog addFrog(float x, float y) {
        ServerFrog frog = new ServerFrog(id++, x, y, worldBodyUtils);
        entities.add(frog);
        return frog;
    }

    public ServerFly addFly(float x, float y) {
        ServerFly fly = new ServerFly(id++, x, y, worldBodyUtils);
        entities.add(fly);
        return fly;
    }

    public ServerBlob addBlob(float x, float y) {
        ServerBlob blob = new ServerBlob(id++, x, y, worldBodyUtils);
        entities.add(blob);
        return blob;
    }

    public void update(float delta) {
        runTime += delta;

        // Reuse audio message
        audio.reset();

        if (pauseGame) {
            runTime = 0;
        }

        // Wait before resuming the game
        if (runTime > 0.2f && !gameOver) {
            // Update game logic
            for (ServerEntity entity : entities) {
                entity.update(delta);
            }
            checkWon();
            if (!allowConnections) {
                // Read <map>.txt
                loader.loadNextLine(delta);
            }
            // Add new entities from worldBodyUtils
            entities.addAll(worldBodyUtils.entities);
            worldBodyUtils.entities.clear();
        }
        if (runTime > 0.2f || gameOver) {
            // Update physics
            world.step(delta, 1, this);
        }

        sendGameStateMessage();

        // Process the messages updated by the StateProcessor
        processEvents(serverListener, incomingEventQueue);

        // Process the sent messages internally within the server
        processEvents(outgoingEventListener, outgoingEventQueue);
    }

    private void checkWon() {
        if (!allowConnections)
            return;
        for (ServerEntity entity: entities) {
            if (allowConnections && (entity instanceof ServerPlayer) &&
                    (((ServerPlayer)entity).score >= WINNING_SCORE)) {
                if (gameOver) {
                    gameOverMessage += " ,";
                }
                gameOverMessage += ((ServerPlayer) entity).getName();
                gameOver = true;
            }
        }
        if (gameOver) {
            gameOverMessage += " won!!!";
            setCurrentOverlay(gameOverMessage);
            server.sendToAllTCP(new GameOverMessage());
            pauseGame = true;
        }
    }

    private void sendGameStateMessage() {
        // Create a master state message for all entities
        GameStateMessage gameStateMessage = MessageObjectPool.instance.
                gameStateMessagePool.obtain();
        for (ServerEntity entity: entities) {
            EntityState state = MessageObjectPool.instance.
                    entityStatePool.obtain();
            entity.updateState(state);
            gameStateMessage.addNewState(state);
        }
        gameStateMessage.time = TimeUtils.nanoTime();

        // Send GameStateMessage and the audio to be played to all clients
        server.sendToAllTCP(gameStateMessage);
        if (audio.audio != 0) {
            server.sendToAllTCP(audio);
        }
    }

    public World getWorld() {
        return world;
    }
    
    
    public ArrayList<ServerEntity> getEntities() {
        return entities;
    }

    public void loadLevel(String level) {
        TiledMap map = new TmxMapLoader().load(level);
        TiledMapTileLayer layer = (TiledMapTileLayer) map.
                getLayers().get("terrain");
        MapLayer playerSpawnPoints = map.getLayers().get("spawnpoints");
        for (MapObject spawnPoints: playerSpawnPoints.getObjects()) {
            Rectangle rectangle = ((RectangleMapObject) spawnPoints).
                    getRectangle();
            worldBodyUtils.playerPositions.add(rectangle.getPosition(new Vector2()).
                    add(rectangle.getWidth() * 0.5f,
                            rectangle.getHeight() * 0.4f));
        }

        WorldRenderer.VIEWPORT_WIDTH =
                (int) (layer.getTileWidth() * layer.getWidth());
        WorldRenderer.VIEWPORT_HEIGHT =
                (int) (layer.getTileHeight() * layer.getHeight());

        MapLayer collision =  map.getLayers().get("collision");
        for (MapObject object: collision.getObjects()) {
            worldManager.createWorldObject(object);
        }
        worldManager.level = level;
        map.dispose();
    }

    public void pauseGame() {
        pauseGame = true;
    }

    public void resumeGame() {
        pauseGame = false;
    }

    public void setCurrentOverlay(String overlayMessage) {
        currentOverlayMessage.message = overlayMessage;
        server.sendToAllTCP(currentOverlayMessage);
    }

    private class WorldManagerServerListener extends Listener {
        
        @Override
        public void connected(Connection connection) {
        }

        @Override
        public void received(Connection connection, Object object) {
            try {
                if (object instanceof ControlsMessage) {
                    ControlsMessage controls = (ControlsMessage) object;
                    if (pauseGame && !gameOver) {
                        if (controls.jump()) {
                            pauseGame = false;
                            currentOverlayMessage.message = "";
                            server.sendToAllTCP(currentOverlayMessage);
                        }

                    } else {
                        playerList.get(connection.getID()).
                                setCurrentControls(controls);
                    }
                } else if (object instanceof ClientDetailsMessage) {
                    if (gameOver) {
                        ServerStatusMessage message = new ServerStatusMessage();
                        message.status = Status.DISCONNECT;
                        message.toastText = "Game Over! Restart the server";
                        server.sendToTCP(connection.getID(), message);
                        return;
                    }
                    ServerPlayer player = new ServerPlayer(id++, 0, 0, worldBodyUtils);
                    playerList.put(connection.getID(), player);
                    entities.add(player);

                    updateClientDetails(connection, (ClientDetailsMessage)object);

                    server.sendToTCP(connection.getID(), currentOverlayMessage);
                    LoadMapMessage loadMapMessage = new LoadMapMessage();
                    loadMapMessage.map = level;
                    server.sendToTCP(connection.getID(), loadMapMessage);
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void disconnected(Connection connection) {
            if (gameOver) {
                return;
            }
            ServerPlayer player = playerList.get(connection.getID());
            if (player != null) {
                player.dispose();
                playerList.remove(connection.getID());
                entities.remove(player);
                destroyObject(player.id);
                server.sendToAllTCP(player.getName() + " has left the game");
            }
        }
    }

    public void destroyObject(short id) {
        DestroyObjectMessage destroyObjectMessage =
                MessageObjectPool.instance.destroyObjectMessagePool.obtain();
        destroyObjectMessage.id = id;
        server.sendToAllTCP(destroyObjectMessage);
    }

    private void updateClientDetails(Connection connection,
                                     ClientDetailsMessage clientDetailsMessage) {
        int version = clientDetailsMessage.protocolVersion;
        if (version != Constants.PROTOCOL_VERSION) {
            ServerStatusMessage message = new ServerStatusMessage();
            message.status = Status.DISCONNECT;
            if (version > Constants.PROTOCOL_VERSION) {
                message.toastText = "Please update server";
            } else if (version < Constants.PROTOCOL_VERSION) {
                message.toastText = "Please update client";
            }
            server.sendToTCP(connection.getID(), message);
            return;
        }

        // Update ServerPlayer
        ServerPlayer player = playerList.get(connection.getID());
        player.setName(clientDetailsMessage.name);
        player.setPlayerSprite(clientDetailsMessage.playerSprite);

        // Send player names to all clients
        PlayerNamesMessage players = new PlayerNamesMessage();
        for (ServerPlayer tempPlayer: playerList.values()) {
            players.players.put(tempPlayer.id, tempPlayer.getName());
        }
        server.sendToAllTCP(players);

        // Send player color to all clients
        PlayerColorsMessage playerColors = new PlayerColorsMessage();
        for (ServerPlayer tempPlayer: playerList.values()) {
            playerColors.colors.put(tempPlayer.id, tempPlayer.getPlayerSprite());
        }
        server.sendToAllTCP(playerColors);
    }
    
    private void processEvents(Listener listener, ArrayList<Event> queue) {
        for (Event event: queue) {
            if (event.state == State.CONNECTED) {
                listener.connected(dummyConnection);
            } else if (event.state == State.RECEIVED) {
                listener.received(dummyConnection, event.object);
            } else if (event.state == State.DISCONNECTED) {
                listener.disconnected(dummyConnection);
            }
        }
        queue.clear();
    }
    
    public ArrayList<Event> getIncomingEventQueue() {
        return incomingEventQueue;
    }

    public ArrayList<Event> getOutgoingEventQueue() {
        return outgoingEventQueue;
    }


    public void createWorldObject(MapObject object) {
        worldBodyUtils.createWorldObject(object);
    }
    
    public void destroyBody(Body body) {
        entities.remove(body.getUserData());
        destroyObject(body.getUserData().id);
    }

    public void dispose() {
    }
    
}

