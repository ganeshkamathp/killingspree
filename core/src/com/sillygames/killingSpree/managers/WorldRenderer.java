package com.sillygames.killingSpree.managers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sillygames.killingSpree.KillingSpree;
import com.sillygames.killingSpree.clientEntities.*;
import com.sillygames.killingSpree.controls.InputController;
import com.sillygames.killingSpree.controls.onScreenControls;
import com.sillygames.killingSpree.helpers.EntityUtils;
import com.sillygames.killingSpree.networking.ClientStateProcessor;
import com.sillygames.killingSpree.networking.ControlsSender;
import com.sillygames.killingSpree.networking.messages.ControlsMessage;
import com.sillygames.killingSpree.networking.messages.EntityState;
import com.sillygames.killingSpree.networking.messages.GameStateMessage;
import com.sillygames.killingSpree.physics.WorldDebugRenderer;
import com.sillygames.killingSpree.renderers.HUDRenderer;
import com.sillygames.killingSpree.renderers.OverlayRenderer;

import java.util.concurrent.ConcurrentHashMap;

public class WorldRenderer {
    
    private static final boolean DEBUG = false;
    public static int VIEWPORT_WIDTH = 525;
    public static int VIEWPORT_HEIGHT = 375;
    private OrthographicCamera camera;
    private OrthogonalTiledMapRenderer renderer;
    private FitViewport viewport;
    private TiledMap map;
    private SpriteBatch batch;
    private ControlsSender controlsSender;
    public ClientStateProcessor clientStateProcessor;
    private ConcurrentHashMap<Short, ClientEntity> worldMap;
    private onScreenControls controls;
    private short recentId;
    private float screenShakeX;
    private float screenShakeY;
    private float screenShakeTime;
    private KillingSpree game;
    public HUDRenderer hudRenderer;
    private byte previousButtonPresses;
    private WorldDebugRenderer debugRenderer;
    private OverlayRenderer overlayRenderer;

    public WorldRenderer(ClientStateProcessor clientStateProcessor,
                         KillingSpree game, String name, String currentPlayerSprite) {
        worldMap = new ConcurrentHashMap<Short, ClientEntity>();
        camera = new OrthographicCamera();
        batch = new SpriteBatch();
        controlsSender = new ControlsSender();
        recentId = -2;
        screenShakeX = 0;
        screenShakeY = 0;
        screenShakeTime = 0;
        hudRenderer = new HUDRenderer();
        this.game = game;
        this.clientStateProcessor = clientStateProcessor;
        clientStateProcessor.setWorldMap(worldMap);

        name = name.trim();
        if (name.length() == 0)
            name = "noname";
        if (name.length() >= 10){
            name = name.substring(0, 10);
        }
        clientStateProcessor.sendClientDetails(name, currentPlayerSprite);
        controls = new onScreenControls();

        overlayRenderer = new OverlayRenderer();
    }

    public boolean loadMap(String level) {
        map = new TmxMapLoader().load(level);
        TiledMapTileLayer layer = (TiledMapTileLayer) map.
                getLayers().get("terrain");
        VIEWPORT_WIDTH = (int) (layer.getTileWidth() * layer.getWidth());
        VIEWPORT_HEIGHT = (int) (layer.getTileHeight() * layer.getHeight());
        viewport = new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT, camera);
        renderer = new OrthogonalTiledMapRenderer(map);
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        return true;
    }

    public void setDebugRenderer(WorldDebugRenderer debugRenderer) {
        this.debugRenderer = debugRenderer;
    }


    public void render(float delta) {
        processGameStateMessage();

        if (renderer == null) {
            if (clientStateProcessor.level != null) {
                loadMap(clientStateProcessor.level);
            }
            return;
        }

        setScreenShake(delta);

        camera.setToOrtho(false, VIEWPORT_WIDTH + screenShakeX,
                VIEWPORT_HEIGHT + screenShakeY);
        viewport.apply();


        renderer.setView(camera);
        renderer.render();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        renderObjects(delta);
        batch.end();

        if (debugRenderer != null) {
            debugRenderer.render(camera.combined);
        }

        processControls();

        if (!InputController.instance.controllerEnabled() &&
                Gdx.app.getType() != Application.ApplicationType.Desktop) {
            controls.render();
        }
        overlayRenderer.render(batch, clientStateProcessor.currentOverlayMessage);
    }

    private void setScreenShake(float delta) {
        if (screenShakeTime > 0) {
            screenShakeTime += delta;
            screenShakeX += MathUtils.random(-7, 7);
            screenShakeY += MathUtils.random(-7, 7);
            if (Math.abs(screenShakeX) > 10) {
                screenShakeX = Math.signum(screenShakeX) * 10;
            }
            if (Math.abs(screenShakeY) > 5) {
                screenShakeY = Math.signum(screenShakeY) * 5;
            }
            if (screenShakeTime > 0.2f) {
                screenShakeTime = 0;
                screenShakeX = 0;
                screenShakeY = 0;
            }
        }

    }

    private void renderObjects(float delta) {
        for (ClientEntity entity: worldMap.values()) {
            if (entity.destroy && entity instanceof ClientBomb)
                shakeScreen();
            if (entity.remove) {
                worldMap.remove(entity.id);
                continue;
            }
            entity.render(delta, batch);
        }
    }

    private void processGameStateMessage() {
        GameStateMessage nextStateMessage;
        float alpha = clientStateProcessor.processStateQueue();
        nextStateMessage = clientStateProcessor.getNextState();

        addNewActors(nextStateMessage);

        for (EntityState state: nextStateMessage.states) {
            if (worldMap.get(state.id) != null) {
                worldMap.get(state.id).processState(state, alpha);
            }
        }
    }

    private void addNewActors(GameStateMessage nextStateMessage) {
        for (EntityState state: nextStateMessage.states) {
            short id = recentId;
            if (!worldMap.containsKey(state.id) && state.id > recentId) {
                ClientEntity entity = null;
                if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.PLAYER) {
                    entity = new ClientPlayer(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.BLOB) {
                    entity = new ClientBlob(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.ARROW) {
                    entity = new ClientArrow(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.BULLET) {
                    entity = new ClientBullet(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.FLY) {
                    entity = new ClientFly(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.FROG) {
                    entity = new ClientFrog(state.id, state.x, state.y, this);
                } else if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.BOMB) {
                    entity = new ClientBomb(state.id, state.x, state.y, this);
                } else {
                    Gdx.app.log("Error", "Couldn't decode actor type");
                    Gdx.app.exit();
                }
                worldMap.put(state.id, entity);
                id = (short) Math.max(id, state.id);

                if (EntityUtils.ByteToActorType(state.type) == EntityUtils.ActorType.PLAYER) {
                    clientStateProcessor.processPlayers();
                }
            }
            recentId = id;
        }
    }

    private void processControls() {
        ControlsMessage message = controlsSender.sendControls(controls);
        
        if (previousButtonPresses != message.buttonPresses) {
            clientStateProcessor.sendUDP(message);
            previousButtonPresses = message.buttonPresses;
        }
        
        if (controls.closeButton()) {
            clientStateProcessor.disconnected = true;
        } else if (clientStateProcessor.gameOver && controls.buttonA()) {
            clientStateProcessor.disconnected = true;
        }
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
        viewport.apply();
        camera.update();
    }
    
    public void shakeScreen() {
        screenShakeTime = 0.01f;
    }

    public void dispose() {
        batch.dispose();
        hudRenderer.dispose();
        overlayRenderer.dispose();
        if (map != null) {
            map.dispose();
            renderer.dispose();
        }
        clientStateProcessor.disconnect();
    }

}
