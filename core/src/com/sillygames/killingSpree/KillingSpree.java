package com.sillygames.killingSpree;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.sillygames.killingSpree.pool.AssetLoader;
import com.sillygames.killingSpree.screens.SplashScreen;
import com.sillygames.killingSpree.sound.SFXPlayer;

public class KillingSpree extends Game {

    Screen nextScreen;
    Screen currentScreen;
    private int width;
    private int height;
    private FreeTypeFontGenerator generator;
    FreeTypeFontParameter parameter;
    public PlatformServices platformServices;
    public SFXPlayer audioPlayer;

    public KillingSpree(PlatformServices platformServices) {
        super();
        this.platformServices = platformServices;
    }
    
	@Override
	public void create () {
	    generator = new FreeTypeFontGenerator
	            (Gdx.files.internal("fonts/splash.ttf"));
	    parameter = new FreeTypeFontParameter();
	    
	    AssetLoader.instance.loadAll();
	    currentScreen = new SplashScreen(this);
	    Gdx.audio.getClass();
        setScreen(currentScreen);
        audioPlayer = new SFXPlayer();
//	    GameScreen gameScreen = new GameScreen(this);
//        gameScreen.startServer();
//        gameScreen.initializeRenderer("maps/retro-small.tmx", "localhost");
//        currentScreen = gameScreen;
	}

    @Override
    public void dispose() {
        super.dispose();
        generator.dispose();
    }

    public BitmapFont getFont(int size) {
        parameter.size = size;
        return generator.generateFont(parameter);
    }

    public void toast(String message) {
        platformServices.toast(message);
    }
}
